CREATE DATABASE jutgitb;

\c jutgitb

CREATE TABLE problemes (
    id_problema SERIAL PRIMARY KEY,
    tema VARCHAR(255) NOT NULL,
    titol VARCHAR(255) NOT NULL,
    enunciat VARCHAR(255) NOT NULL,
    input_public VARCHAR(255) NOT NULL,
    output_public VARCHAR(255) NOT NULL,
    input_privat VARCHAR(255) NOT NULL,
    output_privat VARCHAR(255) NOT NULL,
    resolt BOOL,
    intentsDescomptar INT,
    intentsTotals INT
);

CREATE TABLE intents_usuari (
    id_intent SERIAL PRIMARY KEY,
    id_problema INT,
    intent VARCHAR(30),
    CONSTRAINT fk_problema
    FOREIGN KEY(id_problema)
    REFERENCES problemes(id_problema)
);