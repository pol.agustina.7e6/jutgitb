import kotlinx.serialization.Serializable

/**
 * Classe que ens permet instanciar un problema.
 *
 * @author Pol Agustina Prats
 */
@Serializable
class Problema(
    val id_problema:Int,
    val tema:String,
    val titol:String,
    val enunciat:String,
    val input_public:String,
    val output_public:String,
    val input_privat:String,
    val output_privat:String
){
    var resolt:Boolean = false
    var intentsDescomptar:Int = 0
    var intentsTotals:Int = 0
    var intents_usuari:MutableList<String> = mutableListOf()

    /**
     * Funció que imprimeix la informació d'un problema.
     *
     * @param n El número del problema que s'està imprimint.
     */
    fun imprimirProblema(n:Int){
        println("Problema ${n}: ${this.titol}")
        println()
        println(this.enunciat)
        println()
        println("Exemple:")
        println("     Input: ${this.input_public}")
        println("     Output: ${this.output_public}")
        println()
        println("El teu input: ${this.input_privat}")
        println()
    }

    /**
     * Funció que permet a l'usuari resoldre un problema.
     */
    fun resoldreProblema(){
        var seguir = true
        do{
            print("Escriu aquí el teu output: ")
            val respostaUsuari = escaner.next()
            this.intents_usuari.add(respostaUsuari)
            BaseDades().afegirIntent(this.id_problema,respostaUsuari)
            this.intentsTotals++
            if(respostaUsuari == this.output_privat) {
                println()
                println("És correcte")
                this.resolt = true
                seguir = false
                this.imprimirEstat()
            }
            else {
                intentsDescomptar++
                println()
                println("És incorrecte")
                println("Vols intentar-ho de nou?")
                println("     1. Si     2. No")
                do{
                    print("Escull una opció vàlida:   ")
                    var intentarDeNou = escaner.next()
                    if(intentarDeNou == "No") seguir = false
                    else if(intentarDeNou != "Si") intentarDeNou = "error"
                }while (intentarDeNou=="error")
            }
            BaseDades().actualitzarProblema(this)
        }while (seguir)
    }

    /**
     * Funció que mostra a l'usuari els intents que ha fet en un problema concret i si l'ha resolt o no.
     */
    fun imprimirEstat(){
        println()
        println("Problema: ${this.titol}")
        println("Quantitat d'intents: ${this.intentsTotals}")
        println("Els teus intents: ")
        for(i in this.intents_usuari.indices) println("   - ${this.intents_usuari[i]}")
        print("Estat: ")
        if(!this.resolt) print("no ")
        println("resolt")
        println()
    }
}