import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.Scanner
import kotlin.system.exitProcess

val escaner = Scanner(System.`in`)

const val contrasenyaProfessor = "ITB2023"

lateinit var alumne: Alumne

var connection: Connection? = null

/**
 * Funció que implementa totes les funcionalitats de JutgITB.
 *
 * @author Pol Agustina Prats.
 */
fun main(){
    val jdbcUrl = "jdbc:postgresql://localhost:5432/jutgitb"
    try {
        connection = DriverManager.getConnection(jdbcUrl)
        if(File("./problemes.json").exists()){
            val problemesJSON = File("./problemes.json").readText()
            val problemes = Json.decodeFromString<List<Problema>>(problemesJSON)
            for(problema in problemes){
                BaseDades().afegirProblema(problema)
            }
            File("./problemes.json").delete()
        }
        println("Benvingut a JutgITB")
        println()
        triarRol()
    } catch (e: SQLException) {
        println("JutgITB ha experimentat un error intern: ${e.errorCode} ${e.message}")
    }
}

/**
 * Funció que determina si l'usuari és alumne o professor.
 */
fun triarRol(){
    println("Identifica't:")
    println("     1- Sóc alumne     2- Sóc professor")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                alumne = Alumne()
                menuAlumne()
            }
            "2" -> {
                println()
                professor()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}

/**
 * Funció que valida si l'usuari és professor.
 */
fun professor(){
    print("Introdueix contrasenya: ")
    val contrasenyaUser = escaner.next()
    if(contrasenyaProfessor == contrasenyaUser) {
        println()
        menuProfesor()
    }
    else {
        println()
        triarRol()
    }
}

/**
 * Funció que gestiona les funcionalitats de l'alumne.
 */
fun menuAlumne(){
    println("MENÚ:")
    println("     1- Seguir amb l’itinerari d’aprenentatge")
    println("     2- Llista problemes")
    println("     3- Consultar històric de problemes resolts")
    println("     4- Ajuda")
    println("     5- Sortir")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                alumne.seguirItinerari()
            }
            "2" -> {
                println()
                llistaProblemes()
            }
            "3" -> {
                println()
                alumne.historicProblemes()
            }
            "4" -> {
                println()
                ajuda()
            }
            "5" -> {
                println()
                println("Fins aviat!")
                exitProcess(0)
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    println()
    menuAlumne()
}

/**
 * Permet llistar els problemes per tema.
 */
fun llistaProblemes(){
    var tema = ""
    println("  Llista de problemes:")
    println("     1- Tipus de dades")
    println("     2- Condicionals")
    println("     3- Bucles")
    println("     4- Llistes")
    println("     5- Tornar al menú")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                tema = "Tipus de dades"
            }
            "2" -> {
                println()
                tema = "Condicionals"
            }
            "3" -> {
                println()
                tema = "Bucles"
            }
            "4" -> {
                println()
                tema = "Llistes"
            }
            "5" -> {
                println()
                menuAlumne()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    Problemes().mostraProblemes(tema)
    llistaProblemes()
    println()
}

/**
 * Mostra informació rellevant sobre JutgITB i n'explica el funcionament a l'usuari.
 */
fun ajuda() {
    println("JutgITB és el software més complert i fet servir arreu per resoldre i corregir problemes. Gràcies a la seva simplicitat")
    println("única l'alumne pot gaudir d'una experiència educativa sense precedents.")
    println("El sistema de resolució de problemes és molt senzill. Per cada problema plantejat l'alumne pot decidir si resoldre'l o no.")
    println("En alguns casos es passarà al problema següent i en alguns d'altres l'alumne tindra accés de nou al menú per decidir què")
    println("vol treballar. Al final de cada problema resolt es mostra el recull dels intents que l'alumne ha emprat per arribar a la")
    println("solució.")
    println("En general JutgITB és una aplicació molt intuitiva, encarada precisament a donar una experiència còmode tant a alumnes com a")
    println("professors.")
    println("Bona feina!")
}

/**
 * Funció que gestiona les funcionalitats del professor.
 */
fun menuProfesor(){
    println("MENÚ:")
    println("     1- Afegir nous problemes")
    println("     2- Treure un report de la feina de l'alumne")
    println("     3- Sortir")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                afegirProblema()
                println()
                menuProfesor()
            }
            "2" -> {
                println()
                reportAlumne()
            }
            "3" -> {
                println()
                println("Fins aviat!")
                exitProcess(0)
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}

/**
 * Funció que instancia un objecte nou de la classe Problema i l'afageix serialitzat
 * a l'arxiu JSON de problemes.
 */
fun afegirProblema(){
    val atributsProblemaNom = listOf("tema(\"Tipus de dades\", \"Condicionals\", \"Bucles\" i \"Llistes\")","títol","enunciat","input públic","output públic","input privat","output privat")
    val atributsProblemaValor = mutableListOf<String>()
    for(atribut in atributsProblemaNom){
        print("Introdueix $atribut: ")
        val inputUsuari = escaner.next() + escaner.nextLine()
        atributsProblemaValor.add(inputUsuari)
    }
    val problema = Problema(0,atributsProblemaValor[0],atributsProblemaValor[1],atributsProblemaValor[2],atributsProblemaValor[3],atributsProblemaValor[4],atributsProblemaValor[5],atributsProblemaValor[6])
    BaseDades().afegirProblema(problema)
}

/**
 * Funció que permet veure el progrés de l'alumne de diverses formes.
 */
fun reportAlumne(){
    val problemesResolts = Problemes().resolts(false)
    val problemesResoltsSize = problemesResolts.size
    val problemesPossibles = Problemes().problemes.size
    val intentsDescomptar = intentsDescomptar(problemesResolts)
    val notaSobreDeu = problemesResoltsSize.toDouble()/problemesPossibles.toDouble()*10.0
    println("  Dades a reportar:")
    println("     1- Treure una puntuació en funció dels problemes resolts")
    println("     2- Descomptar per intents")
    println("     3- Mostrar-ho de manera gràfica")
    println("     4- Tornar al menu")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                println("$notaSobreDeu/10")
            }
            "2" -> {
                println()
                println("${notaSobreDeu-(0.1*intentsDescomptar)}/10")
            }
            "3" -> {
                println()
                mostrarGrafic(Problemes().problemes)

            }
            "4" -> {
                println()
                menuProfesor()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    println()
    reportAlumne()
}

/**
 * Funció que retorna els intents que ha fet servir l'alumne per resoldre els problemes.
 *
 * @param problemes Problemes resolts per l'alumne.
 * @return Els intents que ha fet servir l'alumne.
 */
fun intentsDescomptar(problemes: List<Problema>):Int{
    var intentsTotals = 0
    for(problema in problemes){
        if(problema.intentsDescomptar>1)intentsTotals+=problema.intentsDescomptar
    }
    return intentsTotals
}

/**
 * Funció que mostra de manera "gràfica" la feina de l'alumne.
 *
 * @param problemes Problemes resolts per l'alumne.
 */
fun mostrarGrafic(problemes: List<Problema>){
    val intentMesLlarg = intentMesLlarg(problemes)
    val titolMesLlarg = titolMesLlarg(problemes)
    for(problema in problemes){
        print("${espais(titolMesLlarg,problema.titol)}${problema.titol}: ")
        for(intent in problema.intents_usuari){
            print("${espais(intentMesLlarg,intent)}$intent | ")
        }
        println()
    }
}

/**
 * Funció que retorna la quantitat de caràcters que té l'intent amb més caracters.
 *
 * @param problemes Problemes resolts per l'alumne.
 * @return Quantitat de caràcters que té l'intent amb més caracters.
 */
fun intentMesLlarg(problemes: List<Problema>):Int{
    var intentMesLlarg = 0
    for(problema in problemes){
        for(intent in problema.intents_usuari){
            if(intent.length>intentMesLlarg) intentMesLlarg = intent.length
        }
    }
    return intentMesLlarg
}

/**
 * Funció que retorna la quantitat de caràcters que té el títol amb més caracters.
 *
 * @param problemes Problemes resolts per l'alumne.
 * @return Quantitat de caràcters que té el títol amb més caracters.
 */
fun titolMesLlarg(problemes: List<Problema>):Int{
    var titolMesLlarg = 0
    for(problema in problemes){
        if(problema.titol.length>titolMesLlarg)titolMesLlarg=problema.titol.length
    }
    return titolMesLlarg
}

/**
 * Funció que retorna un string amb espais perquè quan es mostri de forma gràfica la
 * feina de l'alumne es vegi ben maco.
 *
 * @param caractersTotals
 * @param caractersConcrets
 * @return És un string que conté tants espais com la diferència entre els caràcters totals i els caràcters en concret.
 */
fun espais(caractersTotals:Int, caractersConcrets:String):String{
    var espais = ""
    for(i in 1..caractersTotals-caractersConcrets.length) espais+=" "
    return espais
}