/**
 * Classe que ens permet operar amb els problemes.
 *
 * @author Pol Agustina Prats
 * @see Problema
 */
class Problemes {

    var problemes = mutableListOf<Problema>()

    /**
     * Funció que inicialitza la variable "problemes" des d'un fitxer JSON.
     */
    init{
        problemes = BaseDades().rebreProblemes()
    }

    /**
     * Funció que filtra els problemes segons si estan resolts o no.
     *
     * @param no
     * @return Llista de problemes resolts o no.
     */
    fun resolts(no:Boolean):List<Problema>{
        val resolts = mutableListOf<Problema>()
        val noResolts = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.resolt) resolts.add(problema)
            else noResolts.add(problema)
        }
        return if(no) noResolts
        else resolts
    }

    /**
     * Funció que mostra els problemes d'un tema concret
     * i permet a l'usuari resoldre'ls.
     *
     * @param tema El tema en concret
     */
    fun mostraProblemes(tema:String){
        val problemesTema = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.tema == tema) problemesTema.add(problema)
        }
        var i = 1
        for (problema in problemesTema) {
            problema.imprimirProblema(i)
            i++
        }
        do {
            println("Vols resoldre algun problema?")
            println("En cas afirmatiu escriu-ne el seu número,")
            print("en qualsevol altre cas escriu 'no': ")
            var respostaUsuari = escaner.next()
            println()
            if(respostaUsuari == "no") llistaProblemes()
            else if(respostaUsuari.toInt() in 1 .. problemesTema.size){
                problemesTema[respostaUsuari.toInt()-1].resoldreProblema()
            }else respostaUsuari = "error"
        }while (respostaUsuari == "error")
    }
}